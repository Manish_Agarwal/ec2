resource "aws_instance" "MyFirstEc2instance_from_terraform" {
  ami = "ami-047a51fa27710816e"
  instance_type = "t2.micro"
  tags = {
      Name = "EC2started_from_terraform"
  }
  key_name = "Keypair-1"
  user_data = <<-EOF
                #!/bin/bash
                yum update -y
                yum install -y httpd
                systemctl start httpd.service
                systemctl enable httpd.service
                echo "Hi Friend , I am $(hostname -f) hosted by Terraform" > /var/www/html/index.html
                EOF
}


